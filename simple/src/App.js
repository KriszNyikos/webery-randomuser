import React, { Component } from 'react';
import DataBox from './components/DataBox/DataBox'
import './App.scss';

export default class App extends Component {
  constructor(props) {
    super()
    this.state = {
      list: []
    }
    this.fetching = this.fetching.bind(this)
    this.primFilter = this.primFilter.bind(this)
    this.isPrime = this.isPrime.bind(this)
  }

  fetching() {
    fetch('https://randomuser.me/api/?results=50')
      .then(resp => resp.json())
      .then(response => {

        let filteredArr = response.results.filter(el => {
          return this.primFilter(el.location.postcode)
        })

        console.log(filteredArr)

        this.setState({ list: filteredArr })
      }
      )
  }

  primFilter(postcode) {

    let postcodeString = postcode.toString().split('')
    let count = 0

    postcodeString.forEach(digit => {

      if (typeof parseInt(digit) === "number") {

        if (this.isPrime(digit)) count++
      }
    })

    if (count >= 2) {
      return true
    } else {
      return false
    }


  }

  isPrime(num) {
    for (var i = 2; i < num; i++)
      if (num % i === 0) return false;
    return num > 1;
  }

  render() {
    return (
      <div className="App">
        <DataBox list={this.state.list} fetch={this.fetching} />
      </div>
    );
  }

}
