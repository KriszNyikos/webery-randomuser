import React, { Component } from 'react'
import "./List.scss"

export default class List extends Component {

    constructor(props) {
        super(props)
        this.state = { startIndex: 0 }
        this.pageButtonCreator = this.pageButtonCreator.bind(this)
    }
   

    startIndexChanger(newIndex){
        this.setState({startIndex: newIndex})
    }

    pageButtonCreator(pages) {
        let buttonContainer = []

        for (let i = 0; i < pages; i++) {
            let newIndex = i * 10
            let button = <button type="button" className="PageButton" onClick={() => this.startIndexChanger(newIndex)}>{i + 1}</button>
            buttonContainer.push(button)
        }
        
        return buttonContainer
    }

    render() {

        let separatedList =  this.props.list.filter((el, index) => {
            if(index >= this.state.startIndex &&  index < this.state.startIndex  + 10 ){
                return true
            }
        })
       
        let pages = this.props.list.length ? Math.ceil(this.props.list.length / 10) : 0

        return (
            <div className="container">
                <table>
                    <thead>
                        <tr>
                            <th>Pic</th>
                            <th>Name</th>
                            <th>Gender</th>
                            <th>Email</th>
                            <th>Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        {separatedList.map((row, key) => {
                            return (
                                <tr key={key}>
                                    <td><img src={row.picture.thumbnail} alt="pic" /></td>
                                    <td>{`${row.name.first} ${row.name.last}`}</td>
                                    <td>{row.gender}</td>
                                    <td>{row.email}</td>
                                    <td>{`${row.location.city}, ${row.location.postcode}, ${row.location.street.name}`}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <div className="pagination">
                    {pages ?  this.pageButtonCreator(pages) : ""}
                </div>
            </div>
        )
    }
}