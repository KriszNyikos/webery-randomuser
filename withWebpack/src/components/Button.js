import React, {Component} from 'react'

export default class Button extends Component{
    constructor(props){
        super()
    }

    render(){
        return(
            <button type="button" onClick={this.props.fetching}>Generate</button>
        )
    }
}