import React, { Component } from 'react'
import List from './List/List'

import "./DataBox.scss"


export default class DataBox extends Component {
    constructor(props) {
        super(props)

        this.state = { 
            gender: "unisex",
                     }
    }

    generate(){
        this.props.fetch()
        this.setState({gender: "all"})
    }

    genderChanger(gender){
        
        this.setState({gender: gender})
        console.log('Gender', this.state.gender)
    }

    render() {

        let filteredList = []

        switch (this.state.gender) {
            case "male":
            filteredList = this.props.list.filter(data => {
                if(data.gender === "male") return true
            })
                break;

            case "female":
                filteredList = this.props.list.filter(data => {
                    if(data.gender === "female") return true
                })
                break;

            default:
                filteredList = this.props.list
                break;
        }


        return (
            <div className="ListAndButtons">
                <div className="row">
                <button type="button" className="GeneratorButton" onClick={() => this.generate()}>Generate</button>
                </div>
                <div className="row">
                <button type="button" className="GenderButton"  onClick={() => this.genderChanger("male")}>Male</button>
                <button type="button" className="GenderButton" onClick={() => this.genderChanger("female")}>Female</button>
                <button type="button" className="GenderButton" onClick={() => this.genderChanger("all")}>All</button>
                </div>
                <div>
                    <List list={filteredList}/>
                </div>
            </div>
        )
    }
}